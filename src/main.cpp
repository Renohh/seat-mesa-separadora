// #include <Arduino.h>

// saidas
const int atuadorMotorLiga    = 52;
const int atuadorRampa1Avanco = 44;
const int atuadorRampa2Avanco = 48;
const int atuadorRampa3Avanco = 46;
const int atuadorRampa1Recuo  = 50;

// entradas
const int sensorCapacitivo    = 53;
const int sensorIndutivo      = 51;
const int sensorDispensador1  = 49;
const int sensorDispensador2  = 47;
const int sensorDispensador3  = 45;
const int sensorRampa1Avanco  = 43;
const int sensorRampa2Avanco  = 41;
const int sensorRampa3Avanco  = 39;
const int sensorRampa1Recuo   = 37;
const int sensorRampa2Recuo   = 35;
const int sensorRampa3Recuo   = 33;

const int botaoLigaDesliga    = 8;
const int botaoReset          = 9;

typedef enum _estados {
  AGUARDA_PECA_INICIO,
  AGUARDA_DISPENSER
} estado;

typedef enum _tipoPeca {
  AUSENTE,
  METAL,
  NAO_METAL
} tipoPeca;

typedef struct _controlePeca {
  tipoPeca tipo;
  int count;
} controlePeca;

#define DESCARTE      (-1)              // indice para peca ser descartada
#define QTD_DISPENSER (3)               // quantidade de dispensers

// variaveis comuns
int maxDispensers   = 3;                      // quantidade maxima de pecas por dispenser
int maxPecasIguais  = 3;                      // quantidade de pecas repetidas
int maxPecas        = 20;                     // quantidade maxima de pecas total
int totalPecas      = 0;                      // quantidade total de pecas
int qtdDispensers[QTD_DISPENSER]  = {};       // quantidade de pecas em cada dispenser
controlePeca ultimaPeca           = {};       // armazena tipo e contador da ultima peca para contagem de duplicadas
int indiceDispensador = DESCARTE;             // indice do dispensador a ser ativado
tipoPeca pecaAtual    = AUSENTE;              // tipo da peca atual
unsigned int tempoDescarte          = 0;      // timer para contar tempo de descarte
unsigned int tempoDescarteCalculado = 10000;  // tempo para a peca atravessar a esteira e cair no descarte
estado estadoAtual = AGUARDA_PECA_INICIO;     // estado atual da execucao

// variaveis auxiliares
const int sensoresDispensador [QTD_DISPENSER] = {sensorDispensador1 , sensorDispensador2  , sensorDispensador3  };
const int sensoresRampaAvanco [QTD_DISPENSER] = {sensorRampa1Avanco , sensorRampa2Avanco  , sensorRampa3Avanco  };
const int sensoresRampaRecuo  [QTD_DISPENSER] = {sensorRampa1Recuo  , sensorRampa2Recuo   , sensorRampa3Recuo   };
const int atuadoresRampaAvanco[QTD_DISPENSER] = {atuadorRampa1Avanco, atuadorRampa2Avanco , atuadorRampa3Avanco };

// prototipos de funcoes
void reset();
void desliga();
void ligaDesliga();
void leBotao();
tipoPeca retornaTipoPeca();
void atuacaoDispensers();
int logicaDeDispenser(tipoPeca peca);

void setup() {
  Serial.begin(115200);
  Serial.println("Inicio");
  pinMode(atuadorRampa1Avanco, OUTPUT);
  pinMode(atuadorRampa1Recuo , OUTPUT);
  pinMode(atuadorRampa2Avanco, OUTPUT);
  pinMode(atuadorRampa3Avanco, OUTPUT);
  pinMode(atuadorMotorLiga   , OUTPUT);

  digitalWrite(atuadorRampa1Avanco, HIGH);
  digitalWrite(atuadorRampa1Recuo , HIGH);
  digitalWrite(atuadorRampa2Avanco, HIGH);
  digitalWrite(atuadorRampa3Avanco, HIGH);
  digitalWrite(atuadorMotorLiga   , LOW);  // inicio com a esteira ligada

  pinMode(sensorDispensador1 , INPUT);
  pinMode(sensorDispensador2 , INPUT);
  pinMode(sensorDispensador3 , INPUT);
  pinMode(sensorIndutivo     , INPUT);
  pinMode(sensorCapacitivo   , INPUT);
  pinMode(sensorRampa1Avanco , INPUT);
  pinMode(sensorRampa1Recuo  , INPUT);
  pinMode(sensorRampa2Avanco , INPUT);
  pinMode(sensorRampa2Recuo  , INPUT);
  pinMode(sensorRampa3Avanco , INPUT);
  pinMode(sensorRampa3Recuo  , INPUT);

  pinMode(botaoLigaDesliga   , INPUT_PULLUP);
  pinMode(botaoReset         , INPUT_PULLUP);

  reset();
}

void loop() {
  if (estadoAtual == AGUARDA_PECA_INICIO) {
    pecaAtual = retornaTipoPeca();  // funcao blocante
    Serial.print("Peca nova = ");
    Serial.println(pecaAtual);

    // sai da funcao apenas quando possui alguma peca, atualizando o estado
    Serial.println("estadoAtual = AGUARDA_DISPENSER");
    estadoAtual = AGUARDA_DISPENSER;

    // seleciona o dispensador
    indiceDispensador = logicaDeDispenser(pecaAtual);
    if (indiceDispensador == DESCARTE) {
      tempoDescarte = millis();
    }
    totalPecas++;
    if (totalPecas >= maxPecas) {
      Serial.println("Total maximo de pecas atingido");
      desliga();
    }
  }

  atuacaoDispensers();
  leBotao();

  return;
}

// reset dos contadores e variaveis gerais
void reset() {
  memset (qtdDispensers, 0, sizeof(qtdDispensers));
  ultimaPeca.tipo = AUSENTE;
  ultimaPeca.count = 0;
  totalPecas = 0;
  pecaAtual = AUSENTE;

  Serial.println("reset");
}

// desliga o motor
void desliga() {
  digitalWrite(atuadorMotorLiga, HIGH);
}

// alterna entre ligar e desligar o motor
void ligaDesliga() {
  digitalWrite(atuadorMotorLiga, not digitalRead(atuadorMotorLiga));
}

// rotina para tratamento de botoes
void leBotao(){
  // se botao liga/desliga pressionado: liga/desliga atuador (rele) do motor
  if(digitalRead(botaoLigaDesliga) == LOW){
    Serial.println("botaoLigaDesliga");
    delay(100);   // tratamento bounce
    while(digitalRead(botaoLigaDesliga) == LOW){
      delay(10);
    }
    ligaDesliga();
  }

  // se botao reset pressionado: reset de contadores e desliga o motor
  if(digitalRead(botaoReset) == LOW){
    Serial.println("botaoReset");
    delay(100);
    while( digitalRead(botaoReset) == LOW){
      delay(10);
    }
    reset();
    desliga();
  }
}

// retorna indice do dispenser
// estando entre 0 e 2 ou -1
//    dispenser 1: metal -> nao metal -> metal
//    dispenser 2: nao metal -> metal -> nao metal
//    dispenser 3: outras peças
//    descartar a partir de 3 peças iguais
//    descartar se ultrapassar limite dos dispensers
int logicaDeDispenser(tipoPeca peca) {
  // default: ir para ultimo dispenser
  int ultimoDispenser = QTD_DISPENSER - 1;
  int indice = ultimoDispenser;

  do {
    // verifica se pecas sao repetidas
    //    atualiza contador
    //    caso seja maior ou igual que maxPecasIguais -> DESCARTE
    // caso contrario atualiza tipo e contador
    if (ultimaPeca.tipo == peca) {
      ultimaPeca.count++;
      if (ultimaPeca.count >= maxPecasIguais) {
        indice = DESCARTE;
        break;
      }
    } else {
      ultimaPeca.tipo = peca;
      ultimaPeca.count = 1;
    }

    // dispenser 1: Metal -> Nao Metal -> ...
    // dispenser 2: Nao Metal -> Metal -> ...
    //    indice 0 + qtd 0 = METAL      par
    //    indice 0 + qtd 1 = NAO_METAL  impar
    //    indice 1 + qtd 0 = NAO_METAL  impar
    //    indice 1 + qtd 1 = METAL      par
    //    => METAL:     indice + qtd = par
    //    => NAO_METAL: indice + qtd = impar
    for (int i = 0; i < ultimoDispenser; i++) {
      Serial.print("i = ");
      Serial.println(i);

      Serial.print("qtdDispensers[i] = ");
      Serial.println(qtdDispensers[i]);

      if ((qtdDispensers[i] < maxDispensers) && ((i + qtdDispensers[i]) % 2) == (peca == METAL ? 0 : 1)) {
        indice = i;
        break;
      }
    }
    if (indice != ultimoDispenser) {
      break;
    }

    // verifica se ultimo dispenser estah cheio
    if (qtdDispensers[ultimoDispenser] >= maxDispensers) {
      indice = DESCARTE;
      break;
    }
  } while (0);

  Serial.print("return indice = ");
  Serial.println(indice);

  return indice;
}

// retorna o tipo da peca
//    AUSENTE: sem nenhuma peca no sensor
//    NAO_METAL: apenas sensor capacitivo ativado
//    METAL: sensor capacitivo e indutivo ativados
// OBS: fica bloqueado aguardando a leitura do sensor capacitivo, apenas tratando os botoes e atuacao de rampas
tipoPeca retornaTipoPeca() {
  tipoPeca tipo = AUSENTE;
  while (1) {
    if (digitalRead(sensorCapacitivo) == LOW) {
      break;
    }
    atuacaoDispensers();
    leBotao();
  }
  while (1) {
    // caso o sensor indutivo seja ativado em algum momento durante a leitura, significa que a peca eh metalica
    // mesmo que ocorram variacoes posteriores deste sensor devido a baixa sensibilidade
    if (digitalRead(sensorIndutivo) == LOW) {
      tipo = METAL;
    }
    // caso tipo permaneca AUSENTE, significa que a peca eh de NAO_METAL
    if (digitalRead(sensorCapacitivo) == HIGH) {
      if (tipo == AUSENTE) {
        tipo = NAO_METAL;
      }
      break;
    }
  }
  return tipo;
}

// controla a logica de atuacao dos atuadores
void atuacaoDispensers() {
  switch (estadoAtual) {
    case AGUARDA_PECA_INICIO:  // aguardando peca no inicio: recua todos os atuadores
      {
        // recua todos os atuadores
        for (int i = 0; i < QTD_DISPENSER; i++) {
          if (digitalRead(sensoresRampaRecuo[i]) != LOW) {
            digitalWrite(atuadoresRampaAvanco[i], HIGH);
            // para recuar rampa1 eh preciso ativar atuador de recuo
            if (i == 0) {
              digitalWrite(atuadorRampa1Recuo, LOW);
            }
          } else {
            // caso esteja recuado, desativar atuador de recuo
            if (i == 0) {
              digitalWrite(atuadorRampa1Recuo, HIGH);
            }
          }
        }
        break;
      }
    case AGUARDA_DISPENSER:  // ativa o dispenser selecionado e le o sensor de rampa, alterando o estado caso a peca passe por ele
      {
        if (indiceDispensador == DESCARTE) {
          if (millis() - tempoDescarte > tempoDescarteCalculado) {
            Serial.println("estadoAtual = AGUARDA_PECA_INICIO");
            estadoAtual = AGUARDA_PECA_INICIO;
          }
        } else {
          // caso a peca seja detectada no dispensador, retorna ao estado inicial
          if (digitalRead(sensoresDispensador[indiceDispensador]) == LOW) {
            Serial.println("estadoAtual = AGUARDA_PECA_INICIO");
            estadoAtual = AGUARDA_PECA_INICIO;
            qtdDispensers[indiceDispensador]++;
          } else {
            // caso o dispensador nao esteja avancado, ativa-o
            if (digitalRead(sensoresRampaAvanco[indiceDispensador]) != LOW) {
              digitalWrite(atuadoresRampaAvanco[indiceDispensador], LOW);
              if (indiceDispensador == 0) {
                digitalWrite(atuadorRampa1Recuo, HIGH);
              }
            } else {
              // rampa1 nao precisa manter avancada
              if (indiceDispensador == 0) {
                digitalWrite(atuadorRampa1Avanco, HIGH);
              }
            }
          }
        }
        break;
      }
  }
}
