#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <assert.h>

#define true          (1)
#define false         (0)
#define HIGH          (1)
#define LOW           (0)
#define OUTPUT        (1)
#define INPUT         (0)
#define INPUT_PULLUP  (1)
void pinMode(int a, int b) {
    return;
}
int digitalRead(int a) {
    return 0;
}

void digitalWrite(int a, int b) {
    return;
}

unsigned int millis() {
    return 0;
}

void delay (int x) {
    return;
}

class _Serial
{
  public:
    void begin(unsigned long baud) {
        return;
    }
    void print(const char* str) {
        return;
    }
    void println(const char* str) {
        return;
    }
    void println(int x) {
        return;
    }
};

_Serial Serial;

// incluir arquivo arduino para testar
#include "../src/main.cpp"

#define assert2(_Expression) \
 (void) \
 ((!!(_Expression)) || \
  (_assert(#_Expression,__FILE__,line),0))
  
void comparaAssert(tipoPeca p, int dispenserExpected, int line) {
    int dispenser = logicaDeDispenser(p);
    assert2(dispenser == dispenserExpected);
    qtdDispensers[dispenser] ++;
}

// funcao main
int main () {
    reset();
    comparaAssert(METAL      , 0, __LINE__);
    comparaAssert(METAL      , 2, __LINE__);
    comparaAssert(METAL      , -1, __LINE__);
    comparaAssert(METAL      , -1, __LINE__);
    comparaAssert(METAL      , -1, __LINE__);
    
    reset();
    comparaAssert(NAO_METAL  , 1, __LINE__);
    comparaAssert(NAO_METAL  , 2, __LINE__);
    comparaAssert(NAO_METAL  , -1, __LINE__);
    comparaAssert(NAO_METAL  , -1, __LINE__);
    comparaAssert(NAO_METAL  , -1, __LINE__);
    
    reset();
    comparaAssert(NAO_METAL  , 1, __LINE__);
    comparaAssert(NAO_METAL  , 2, __LINE__);
    comparaAssert(NAO_METAL  , -1, __LINE__);
    comparaAssert(NAO_METAL  , -1, __LINE__);
    comparaAssert(NAO_METAL  , -1, __LINE__);
    
    reset();
    comparaAssert(METAL      , 0, __LINE__);
    comparaAssert(NAO_METAL  , 0, __LINE__);
    comparaAssert(METAL      , 0, __LINE__);
    comparaAssert(NAO_METAL  , 1, __LINE__);
    comparaAssert(METAL      , 1, __LINE__);
    comparaAssert(NAO_METAL  , 1, __LINE__);
    comparaAssert(METAL      , 2, __LINE__);
    comparaAssert(NAO_METAL  , 2, __LINE__);
    comparaAssert(METAL      , 2, __LINE__);
    comparaAssert(NAO_METAL  , -1, __LINE__);
    comparaAssert(METAL      , -1, __LINE__);
    comparaAssert(NAO_METAL  , -1, __LINE__);
    comparaAssert(METAL      , -1, __LINE__);

    reset();
    comparaAssert(NAO_METAL  , 1, __LINE__);
    comparaAssert(NAO_METAL  , 2, __LINE__);
    comparaAssert(NAO_METAL  , -1, __LINE__);
    comparaAssert(NAO_METAL  , -1, __LINE__);
    comparaAssert(NAO_METAL  , -1, __LINE__);
    
    reset();
    comparaAssert(NAO_METAL  , 1, __LINE__);
    comparaAssert(METAL      , 0, __LINE__);
    comparaAssert(NAO_METAL  , 0, __LINE__);
    comparaAssert(METAL      , 0, __LINE__);
    comparaAssert(NAO_METAL  , 2, __LINE__);
    comparaAssert(METAL      , 1, __LINE__);
    comparaAssert(NAO_METAL  , 1, __LINE__);
    comparaAssert(METAL      , 2, __LINE__);
    comparaAssert(NAO_METAL  , 2, __LINE__);
    comparaAssert(METAL      , -1, __LINE__);
    comparaAssert(NAO_METAL  , -1, __LINE__);
    comparaAssert(METAL      , -1, __LINE__);
    comparaAssert(NAO_METAL  , -1, __LINE__);
    comparaAssert(METAL      , -1, __LINE__);
    return 0;
}
